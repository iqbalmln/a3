module quickstart

go 1.19
require(
  go.mongodb.org/mongo-driver v1.10.2 // indirect
  gopkg.in/mgo.v2 v2.0.0-20190816093944-a6b53ec6cb22
)
